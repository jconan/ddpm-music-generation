from typing import Union
import numpy as np
import librosa
from PIL import Image
from diffusers import AudioDiffusionPipeline, Mel, UNet2DModel, DDPMScheduler, DDIMScheduler
from torchvision.transforms import ToTensor, Normalize, Compose
import os
import pandas as pd
from datasets import Dataset, DatasetDict, Features, Image as ds_Image, Value, load_from_disk
import torch

def audio_from_mel_spectogram(
    mel:Union[Image.Image, np.ndarray], 
    sample_rate:int=11025, 
) -> np.ndarray:
    """
    Convert a mel spectogram to audio.
    Args:
        mel (np.ndarray): mel spectogram
        sample_rate (int): sample rate of audio
    Returns:
        np.ndarray: audio
    """
    if not isinstance(mel, Image.Image):
        mel = Image.fromarray(mel)
    
    audio = Mel(
        x_res = mel.height,
        y_res = mel.width,
        hop_length = 512,
        sample_rate = sample_rate,
        n_fft = 2048,
    ).image_to_audio(image=mel)

    return audio

def load_train_dataset(path:str) -> torch.utils.data.Dataset:
    """
    Load a dataset from a path.
    Args:
        path (str): path to dataset
    Returns:
        torch.utils.data.Dataset: dataset
    """
    dataset = load_from_disk(os.path.join(path, 'train'))

    augmentations = Compose([
        ToTensor(),
        Normalize([0.5], [0.5]),
    ])
    def transforms(ex): return {"input":[augmentations(image) for image in ex["image"]]}
        
    dataset.set_transform(transforms)

    train_dataloader = torch.utils.data.DataLoader(
            dataset, batch_size=1, shuffle=True)
            
    return train_dataloader


def model_from_scratch(ddpm:bool=True) -> AudioDiffusionPipeline:
    """
    Create a model from scratch.
    Args:
        ddpm (bool): use DDPM or DDIM
    Returns:
        AudioDiffusionPipeline: model
    """
    # architecture of the UNet model
    # inspired by teticio/audio-diffusion on GitHub
    unet = UNet2DModel(
        sample_size=(256, 256), # we generate spectograms of size 256
        in_channels=1,
        out_channels=1,
        layers_per_block=2,
        block_out_channels=(128, 128, 256, 256, 512, 512),
        down_block_types=(
            "DownBlock2D",
            "DownBlock2D",
            "DownBlock2D",
            "DownBlock2D",
            "AttnDownBlock2D",
            "DownBlock2D",
        ),
        up_block_types=(
            "UpBlock2D",
            "AttnUpBlock2D",
            "UpBlock2D",
            "UpBlock2D",
            "UpBlock2D",
            "UpBlock2D",
        ),
    )

    mel = Mel(
        x_res = 256,
        y_res = 256,
        hop_length = 512,
        sample_rate = 11025,
        n_fft = 2048,
    )

    NoiseScheduler = DDPMScheduler if ddpm else DDIMScheduler
    noise_scheduler = NoiseScheduler(
        num_train_timesteps=1000,
    )

    return AudioDiffusionPipeline(
        unet=unet,
        mel=mel,
        scheduler=noise_scheduler,
        vqvae=None,
    )
