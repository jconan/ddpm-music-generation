from typing import Iterable, Tuple, Union, List, Callable
import os

import torch
from datetime import datetime
import numpy as np
from PIL import Image
from tqdm.auto import tqdm
from diffusers import AudioDiffusionPipeline, UNet2DModel, Mel, DDIMScheduler, DDPMScheduler 
from librosa.util import normalize
from diffusers.optimization import get_scheduler
from .utils import model_from_scratch
from accelerate import Accelerator
import scipy.io.wavfile as wav



class ETH_Music_Diffusion:

    def __init__(self, model:str = None, optimizer:torch.optim.Optimizer = torch.optim.AdamW, progress_bar: Iterable = tqdm, ddpm:bool=True) -> None:
        """Class for generating audio using De-noising Diffusion Probabilistic Models.

        Args:
            model (String): name of model (local directory or Hugging Face Hub) or None
            progress_bar (iterable): iterable callback for progress updates or None
            ddpm (bool): use DDPM or DDIM as a diffusion model
        """

        self.pipe:AudioDiffusionPipeline 
        self.optimizer:torch.optim.Optimizer = optimizer

        self.pipe = AudioDiffusionPipeline.from_pretrained(model) if model else model_from_scratch(ddpm)
        
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.generator = torch.Generator(self.device)
        self.pipe.to(self.device)

        self.progress_bar = progress_bar or (lambda _: _) # if progress_bar is None, return the input

    @property
    def unet(self) -> UNet2DModel:
        return self.pipe.unet
    @property
    def mel(self) -> Mel:
        return self.pipe.mel
    @property
    def scheduler(self) -> Union[DDIMScheduler, DDPMScheduler]:
        return self.pipe.scheduler
    
    # generate spectrogram and audio randomly
    def generate_audio(
        self,
        batch_size: int = 1,
        steps: int = None,
        eta: float = 0,
        noise: np.ndarray = None,
    ) -> Tuple[Image.Image, Tuple[int, np.ndarray]]:
        """
        Generate audio using the model.
        
        Args:
            batch_size (int): number of audio files to generate
            steps (int): number of steps to run the diffusion process
            eta (float): eta value for the diffusion process
            noise (np.ndarray): noise to use for the diffusion process
            generator (torch.Generator): generator for the diffusion process
            step_generator (torch.Generator): generator for the diffusion process
        Returns:
            Tuple[Image.Image, Tuple[int, np.ndarray]]: image and audio
        """

        images, (sample_rate, audios) = self.pipe(
            batch_size=batch_size,
            steps=steps,
            generator=self.generator,
            eta=eta,
            noise=noise,
            return_dict=False,
        )
        return images[0], (sample_rate, audios[0])


    # take an audio, add noise, and then denoise it to generate multiple samples 
    def generate_audio_from_audio(
        self,
        audio: Union[np.ndarray, torch.Tensor] = None,
        nb:int=4,
        steps_to_noise: torch.Tensor = torch.tensor(50, dtype=int),
        steps: int = None,
        start_step: int = 0,
        eta: float = 0,
    ) -> List[Tuple[Image.Image, Tuple[int, np.ndarray]]]:
        """
        Generate audio from audio using the model. Audio must be transformed to a torch.Tensor.

        Args:
            audio (Union[Image.Image, np.ndarray, torch.Tensor]): audio to use for the diffusion process
            nb (int): number of audio files to generate
            steps_to_noise (int): number of steps to run the diffusion process
            steps (int): number of steps to run the diffusion process
            start_step (int): number of steps to run the diffusion process
            eta (float): eta value for the diffusion process
        Returns:
            List[Tuple[Image.Image, Tuple[int, np.ndarray]]]: list of images and audio

        """
            
        if not isinstance(steps_to_noise, torch.Tensor):
            steps_to_noise = torch.tensor(steps_to_noise, dtype=int)

        if isinstance(audio, np.ndarray):
            audio = torch.from_numpy(audio)

        if not isinstance(audio, torch.Tensor):
            raise TypeError("audio must be a torch.Tensor, numpy.ndarray, or PIL.Image.Image")

        audio = audio.to(self.device)

        noise = torch.randn(
            (
                1,
                self.unet.in_channels,
                self.unet.sample_size[0],
                self.unet.sample_size[1],
            ),
            device=self.device,
            generator=None 
        )

        
        noisy_sample = self.scheduler.add_noise(audio, noise, timesteps=steps_to_noise)        
        # create a batch of length nb 
        noisy_sample = noisy_sample.repeat(nb, 1, 1, 1)

        out = self.pipe(
            noise=audio,
            batch_size=nb,
            start_step=start_step,
            steps=steps,
            eta=eta,
            return_dict=False,
        )
        return out


    # take two audio, interpolate between them, and then denoise it to generate a new sample
    def generate_audio_from_audio_interpolate(
        self,
        x1: Union[np.ndarray, torch.Tensor],
        x2: Union[np.ndarray, torch.Tensor],
        alpha: float=0.5,
        steps: int = None,
        eta: float = 0,
    ) -> Tuple[Image.Image, Tuple[int, np.ndarray]]:
        """
        Generate audio from audios using the model. Audios must be transformed to a torch.Tensor. 
        Uses slerp() method from pipeline to interpolate.

        Args:
            x1 (Union[np.ndarray, torch.Tensor]): audio to use for the diffusion process
            x2 (Union[np.ndarray, torch.Tensor]): audio to use for the diffusion process
            alpha (float): interpolation value
            steps (int): number of steps to run the diffusion process
            eta (float): eta value for the diffusion process
        Returns:
            Tuple[Image.Image, Tuple[int, np.ndarray]]: image and audio
        """

        if isinstance(x1, np.ndarray):
            x1 = torch.from_numpy(x1)
        if isinstance(x2, np.ndarray):
            x2 = torch.from_numpy(x2)

        if not isinstance(x1, torch.Tensor):
            raise TypeError("x1 must be a torch.Tensor or numpy.ndarray")
        if not isinstance(x2, torch.Tensor):
            raise TypeError("x2 must be a torch.Tensor or numpy.ndarray")

        x1 = x1.to(self.device)
        x2 = x2.to(self.device)

        x = self.pipe.slerp(x1, x2, alpha).unsqueeze(0)

        out = self.pipe(
            noise=x,
            batch_size=1,
            steps=steps,
            eta=eta,
            return_dict=False,
        )
        return out


    def train(
        self, 
        dataset: torch.utils.data.Dataset,
        name: str = None,
        nb_epochs: int = 10,
        batch_size: int = 1,
        optimizer: torch.optim.Optimizer = torch.optim.AdamW,
        lr_scheduler:str = "cosine",
        loss_fn: Callable = torch.nn.functional.mse_loss,
        save_model_epochs: int = 5,
        gen_samples_epochs: int = 5,
        tb_logs: str = "logs",
    ) -> None:
        """
        Train the model. Uses Accelerator to train on GPU or TPU.

        Args:
            nb_epochs (int): number of epochs to train
            batch_size (int): batch size to use for training
            optimizer (torch.optim.Optimizer): optimizer to use for training
            lr_scheduler (str): learning rate scheduler to use for training
            dataset (torch.utils.data.Dataset): dataset to use for training
            loss_fn (function): loss function to use for training
            tb_logs (str): the log directory for tensorboard
        Returns:
            None
        """
        run_out_dir = f"training_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S') if not name else name}"
        os.makedirs(run_out_dir)

        optimizer = optimizer(
            self.unet.parameters(),
            lr=1e-4,
            betas=(0.95, 0.999),
            weight_decay=1e-6,
            eps=1e-08,
        )
        lr_scheduler = lr_scheduler = get_scheduler(
            lr_scheduler,
            optimizer=optimizer,
            num_warmup_steps=500, # 500 warmup steps, inspired by teticio on GitHub
            num_training_steps=(len(dataset) * nb_epochs) // 8, # 8 gradient accumulation steps, inspired by teticio on GitHub
        )

        accelerator = Accelerator(
            gradient_accumulation_steps=8,
            mixed_precision = "no",
            log_with="tensorboard",
            logging_dir=os.path.join(run_out_dir, tb_logs)
        )
        accelerator.init_trackers(tb_logs)

        model, optimizer, train_dataloader, lr_scheduler = accelerator.prepare(self.unet, optimizer, dataset, lr_scheduler)

        global_step = 0
        for epoch in range(nb_epochs):
            progress_bar = self.progress_bar(total=len(train_dataloader))
            progress_bar.set_description(f"Epoch {epoch}")

            model.train()
            for step, batch in enumerate(train_dataloader):
                clean_images = batch["input"]
                # Sample noise that we'll add to the images
                noise = torch.randn(clean_images.shape).to(clean_images.device)
                bsz = clean_images.shape[0]
                # Sample a random timestep for each image
                timesteps = torch.randint(
                    0,
                    self.scheduler.num_train_timesteps,
                    (bsz, ),
                    device=clean_images.device,
                ).long()

                # Add noise to the clean images according to the noise magnitude at each timestep
                # (this is the forward diffusion process)
                noisy_images = self.scheduler.add_noise(clean_images, noise, timesteps)

                with accelerator.accumulate(model):

                    # Predict the noise residual
                    noise_pred = model(noisy_images, timesteps)["sample"]
                    loss = loss_fn(noise_pred, noise)
                    accelerator.backward(loss)

                    if accelerator.sync_gradients:
                        accelerator.clip_grad_norm_(model.parameters(), 1.0)
                    optimizer.step()
                    lr_scheduler.step()
                    optimizer.zero_grad()

                progress_bar.update(1)
                global_step += 1

                logs = {
                    "loss": loss.detach().item(),
                    "lr": lr_scheduler.get_last_lr()[0],
                    "step": global_step,
                }

                progress_bar.set_postfix(**logs)
                accelerator.log(logs, step=global_step)

            progress_bar.close()
            accelerator.wait_for_everyone()
            
            if epoch % save_model_epochs == 0: 
                pipeline = AudioDiffusionPipeline(
                    vqvae=None,
                    mel=self.mel, 
                    scheduler=self.scheduler,
                    unet = accelerator.unwrap_model(model),
                )
                pipeline.save_pretrained(run_out_dir)

            if epoch % gen_samples_epochs == 0:
                pipeline = AudioDiffusionPipeline(
                    vqvae=None,
                    mel=self.mel, 
                    scheduler=self.scheduler,
                    unet = accelerator.unwrap_model(model),
                )
                images, (sample_rate, audios) = self.pipe(
                    batch_size=batch_size,
                    steps=1000,
                    generator=self.generator,
                    return_dict=False,
                )

                # Save image and audio
                images = np.array([
                    np.frombuffer(image.tobytes(), dtype="uint8").reshape(
                        (len(image.getbands()), image.height, image.width))
                    for image in images
                ])
                accelerator.trackers[0].writer.add_images(
                    "test_samples", images, epoch)
                for _, audio in enumerate(audios):
                    accelerator.trackers[0].writer.add_audio(
                        f"test_audio_{_}",
                        normalize(audio),
                        epoch,
                        sample_rate=sample_rate,
                    )
            accelerator.wait_for_everyone()

        accelerator.end_training()

    def save_model(self, name):
        save_name = "%s_%s" % str(name) if name else "random_save" , datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        self.pipe.save_pretrained(save_name)
        return save_name

# from mymodels import ETH_Music_Diffusion as ETHMD
# from mymodels.utils import load_train_dataset
# ds= load_train_dataset('.')
# test = ETHMD()
# test.train(ds)